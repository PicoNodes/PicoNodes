* Bytecode Format

** Number
   :PROPERTIES:
   :CUSTOM_ID: numbers
   :END:

A number is a signed two's complement integer between -100 and 100, inclusive.
Arithmetic operations are saturating on overflow, so that ~90+20=100~.

** Register references
   :PROPERTIES:
   :CUSTOM_ID: registers
   :END:

Registers are given IDs outside of the normal number range. This way, the same
instruction can handle both register and immediate values.

*** Registers

|           id | name                     |
|--------------+--------------------------|
|         -128 | up                       |
|         -127 | down                     |
|         -126 | left                     |
|         -125 | right                    |
| -124 to -101 | reserved for IO ports    |
|  -100 to 100 | immediate                |
|   101 to 125 | reserved for data memory |
|          126 | acc                      |
|          127 | null                     |

** Flags
   :PROPERTIES:
   :CUSTOM_ID: flags
   :END:

Each instruction has two flags: ~+~ and ~-~, corresponding to VM flags.
If an instruction has a given flag then the instruction is only executed if
the corresponding VM flag is also true.

** Conditionals
   :PROPERTIES:
   :CUSTOM_ID: conditionals
   :END:

Conditionals set the ~+~ and ~-~ flags according to the following rules:

| instruction | case | +     | -     |
|-------------+------+-------+-------|
| teq         | a=b  | true  | false |
| teq         | a<b  | false | true  |
| teq         | a>b  | false | true  |
|-------------+------+-------+-------|
| tgt         | a=b  | false | true  |
| tgt         | a<b  | false | true  |
| tgt         | a>b  | true  | false |
|-------------+------+-------+-------|
| tlt         | a=b  | false | true  |
| tlt         | a<b  | true  | false |
| tlt         | a>b  | false | true  |
|-------------+------+-------+-------|
| tcp         | a=b  | false | false |
| tcp         | a<b  | false | true  |
| tcp         | a>b  | true  | false |

** Instructions

- R: [[#registers][Register]]
- I: [[#numbers][Immediate]]
- x/y: x OR y
- F: [[#flags][Flag(s)]]
- <number>: Magical constant

|             | 2 bits | 6 bits | 8 bits      | 8 bits         |                   |
| instruction | flags  | opcode | operand A   | operand B      | pseudocode        |
|-------------+--------+--------+-------------+----------------+-------------------|
| mov         | F      |      0 | R/I: source | R: destination | B = A             |
| add         | F      |      1 | R/I         | 0              | acc = acc + A     |
| sub         | F      |      1 | R/I         | 1              | acc = acc - A     |
| teq         | F      |      4 | R/I         | R/I            | See [[#conditionals]] |
| tgt         | F      |      5 | R/I         | R/I            | See [[#conditionals]] |
| tlt         | F      |      6 | R/I         | R/I            | See [[#conditionals]] |
| tcp         | F      |      7 | R/I         | R/I            | See [[#conditionals]] |
