#+TITLE: User Manual for the PICONODE-SWITCH
#+AUTHOR: Therese Kennerberg
# Fancy links
#+LATEX_HEADER: \usepackage{xcolor}
#+LATEX_HEADER: \hypersetup{colorlinks, linkcolor={red!50!black}, citecolor={blue!50!black}, urlcolor={blue!80!black}}

* Introduction

The PicoNode-switch is designed to be used with the PicoNode kit and is compatible with the PicoNode kits motherboard.
It is meant to give a more hands-on and visual understanding of the communication between the PicoNodes.
This node is not programmable by the user but is already running a program designed to be used with the motherboard.

* Features

The PicoNode-switch offers four switches, and can detect one press at a time.

* Getting started

Follow these steps to learn how to use the PicoNode-switch:
1. Power the motherboard
2. Place the PicoNode-switch on a available slot on the motherboard. This slot should also have one available slot to the right.
3. Place the PicoNode-LED on the available slot to the right OR program a PicoNode to receive a value from the left.
4. If you placed a LED then press one button and a LED should light ELSE if you placed a programmed PicoNode, press one of the buttons and a value from 1-4 is transmitted
   to the PicoNode from the PicoNode-switch.

* Hardware and layout

The hardware and the PCB-layout are available in the data sheet provided for the PicoNode kit.

* Switches

The switches are used to symbolize a value transmitted from the node. There is one output pin to the right for transmitting a value from 1 to 4.
The top left button holds the value of one, the bottom left button hold the value of two, the top right button holds the value of three and the bottom right button hold the value of
four. When the button is pressed, it respective value is transmitted to node placed to its right. Only one value can be transmitted at a time and if no button is pressed the value transmitted is 0.
