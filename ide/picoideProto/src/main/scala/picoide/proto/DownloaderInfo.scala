package picoide.proto

import java.util.UUID

case class DownloaderInfo(id: UUID, label: Option[String])
